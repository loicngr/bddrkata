<?php
if (!defined("ROOT")) {
    define("ROOT", "../");
}
/**
 * Controller Class
 *
 * CRUD operation
 */

class Controller
{
    private $model;

    function __construct()
    {
        require ROOT . "Model/Model.php";

        $this->model = new Model();
    }

    function router($query)
    {
        if ($query == "getCategory"):
            if (isset($_GET['id'])):
                $category = $this->model->category($_GET['id']);
                if ($category):
                    echo $category;
                else:
                    echo false;
                endif;
            else:
                $category = $this->model->category();
                if ($category):
                    echo $category;
                else:
                    echo false;
                endif;
            endif;
        elseif ($query == "getSubCategory"):
            if (isset($_GET['name'])):
                $category = $_GET['name'];

                $subCategory = $this->model->subCategory($category);
                if ($subCategory):
                    echo $subCategory;
                else:
                    echo false;
                endif;
            endif;
        elseif ($query == "getRelatedCharacteristics"):
            if (isset($_GET['subcategory'])):
                $subcategory = $_GET['subcategory'];
                $subcategoryAr = explode(',', $subcategory);

                $relatedCharacteristics = $this->model->relatedCharacteristics($subcategoryAr);
                if ($relatedCharacteristics):
                    echo $relatedCharacteristics;
                else:
                    echo false;
                endif;
            endif;
        endif;
    }
}

$ctrl = new Controller();
if (!empty($_GET)):
    if (isset($_GET['q'])):
        $query = htmlspecialchars($_GET['q']);

        if ($query):
            $ctrl->router($query);
        endif;
    endif;
endif;