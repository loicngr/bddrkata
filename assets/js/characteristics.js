/**
 * Get characteristics
 * @return {[]}
 */
async function getRelatedCharacteristics() {
    let characteristics = await request(`getRelatedCharacteristics&subcategory=${selectedSubCategory.join(',')}`);
    if (characteristics.status) {
        return characteristics.data;
    }
    throw new Error(characteristics.data);
}

function relatedCharacteristics() {
    getRelatedCharacteristics().then((characteristics) => {
        generateElement(characteristics, '#characteristics');
    });
}