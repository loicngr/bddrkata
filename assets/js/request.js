async function request(query) {
    let output = {status: false, data: false};
    try {
        let response = await fetch(`./Controller/Controller.php?q=${query}`);
        if (response.ok) {
            output.data = await response.json();
            output.status = true;
        }
    } catch (error) {
        output.data = error;
    }
    return output;
}