/**
 * Get all category
 * @return {array}
 */
async function getCategory() {
    let category = await request('getCategory');
    if (category.status) { return category.data; }
    throw new Error(category.data);
}
/**
 * Get all subcategory
 * @return {array}
 */
async function getSubCategory() {
    let subCategory = await request(`getSubCategory&name=${selectCategory}`);
    if (subCategory.status) { return subCategory.data; }
    throw new Error(subCategory.data);
}

/**
 * On input click (chose category)
 * @param {Event} e
 */
function selectCategory(e) {
    selectCategory = e.target.value;
    selectedSubCategory = [];
    subCategory();
}

/**
 * On input click (chose subcategory)
 * @param {Event} e
 */
function selectSubCategory(e) {
    if (!selectedSubCategory.includes(e.target.value))
        selectedSubCategory.push(e.target.value);
    else if (!e.target.checked && selectedSubCategory.includes(e.target.value)) {
        let index = selectedSubCategory.findIndex((el) => el === e.target.value);
        selectedSubCategory.splice(index, 1);
    }

    relatedCharacteristics();
}

function subCategory() {
    getSubCategory().then((subCategory) => {
        generateElement(subCategory, '#subcategory');
    });
}
function category() {
    getCategory().then((category) => {
        generateElement(category, '#category');
    });
}