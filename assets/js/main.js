let selectedCategory = null;
let selectedSubCategory = [];

/**
 * @param {Element} domRef 
 */
function generateEventListener(domRef, callback) {
    domRef.addEventListener('click', callback, false);
}

function openListSubCategory(e) {
    let items = e.target.nextSibling;
    items.style.display = (items.style.display === 'block') ? 'none' : 'block';
}

/**
 * Generate Dom Element
 * @param {Array} data 
 * @param {String} domRef 
 */
function generateElement(data, domRef) {
    let parent = document.querySelector(domRef);
    let element = domRef.slice(1, domRef.length);

    let title = document.createElement('h1');
    title.innerText = element.charAt(0).toUpperCase() + element.slice(1, element.length);
    parent.innerHTML = "";
    parent.appendChild(title);

    switch (element) {
        case 'category':
            data.forEach(element => {
                let div = document.createElement('div');
                let input = document.createElement('input');
                let label = document.createElement('label');

                // INPUT
                input.setAttribute('type', 'radio');
                input.setAttribute('id', element.name);
                input.setAttribute('value', element.name);
                input.setAttribute('name', element);

                // LABEL
                label.setAttribute('for', element.name);
                label.innerText = element.name;

                // DIV
                div.appendChild(input);
                div.appendChild(label);

                parent.appendChild(div);
                generateEventListener(input, selectCategory);
            });
            break;
        case 'subcategory':
            let div = document.createElement('div');
            let span = document.createElement('span');
            let ul = document.createElement('ul');

            div.classList.add('dropdown-check-list');
            span.innerText = 'Sous catégories';
            data.forEach(element => {
                let li = document.createElement('li');
                let input = document.createElement('input');
                let label = document.createElement('label')

                // INPUT
                input.setAttribute('type', 'checkbox');
                input.setAttribute('id', element.name);
                input.setAttribute('value', element.name);
                input.setAttribute('name', element);

                // LABEL
                label.setAttribute('for', element.name);
                label.innerText = element.name;

                // LI
                li.appendChild(input);
                li.appendChild(label);
                ul.appendChild(li);
                generateEventListener(input, selectSubCategory);
            });

            generateEventListener(span, openListSubCategory);
            div.appendChild(span);
            div.appendChild(ul);
            parent.appendChild(div);
            break;
        case 'characteristics':
            if (data.flat().length !== 0) {
                data.forEach(element => {
                    let div = document.createElement('div');
                    let code = document.createElement('code');

                    code.innerText = JSON.stringify(element);
                    div.appendChild(code);
                    parent.appendChild(div);
                });
            } else {
                parent.innerHTML = "";
            }
            break;
        default:
            break;
    }
}

function main(dom_ref) {
    let div = document.createElement('div');
    div.id = "category";
    document.querySelector(dom_ref).appendChild(div);

    div = document.createElement('div');
    div.id = "subcategory";
    document.querySelector(dom_ref).appendChild(div);

    div = document.createElement('div');
    div.id = "characteristics";
    document.querySelector(dom_ref).appendChild(div);

    category();
}
main('body > form');