<?php
/**
 * Model Class
 *
 * Connect to Database with PDO method
 */

class Model
{
    protected $mysql_config;
    protected $pdo;

    function __construct() {
        require_once ROOT . "env.php";

        $this->mysql_config = array(
            "login" => $MYSQL_LOGIN,
            "pass" => $MYSQL_PASS,
            "db" => $MYSQL_DBNAME,
            "host" => $MYSQL_HOST
        );

        $this->connection();
    }
    /**
     * Connection to my Database with PDO
     */
    private function connection()
    {
        try {
            $this->pdo = new PDO(
                "mysql:host={$this->mysql_config['host']};dbname={$this->mysql_config['db']};charset=utf8",
                $this->mysql_config["login"],
                $this->mysql_config["pass"]
            );
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo json_encode($e->getMessage());
        }
    }

    public function category($id = false)
    {
        if ($id):
            $request = $this->pdo->prepare("SELECT name FROM `category` WHERE id = :id");
            $request->bindParam(":id", intval($id));
            $request->execute();
            $result = $request->fetch(PDO::FETCH_OBJ);
            return json_encode($result);
        else:
            $request = $this->pdo->prepare("SELECT name FROM `category`");
            $request->execute();
            $result = $request->fetchAll(PDO::FETCH_OBJ);
            return json_encode($result);
        endif;
    }

    public function subCategory($category = false)
    {
        if ($category):
            $request = $this->pdo->prepare("SELECT id FROM `category` WHERE name = :name");
            $request->bindParam(":name", $category);
            $request->execute();
            $categoryID = $request->fetch(PDO::FETCH_OBJ);

            $request = $this->pdo->prepare("SELECT * 
                    FROM `category` 
                    INNER JOIN subcategory 
                    ON category.id = subcategory.category_id 
                    WHERE subcategory.category_id = :id"
            );
            $request->bindParam(":id", intval($categoryID->id));
            $request->execute();
            $result = $request->fetchAll(PDO::FETCH_OBJ);
            return json_encode($result);
        endif;
    }

    public function relatedCharacteristics($subcategory = [])
    {
        if ($subcategory):
            $output = array();

            foreach ($subcategory as $elem):
                $request = $this->pdo->prepare("SELECT * 
                    FROM `subcategory` 
                    INNER JOIN `element` 
                    ON subcategory.id = element.subcategory_id 
                    WHERE subcategory.name = :name"
                );
                $request->bindParam(":name", $elem);
                $request->execute();
                $result = $request->fetchAll(PDO::FETCH_OBJ);
                array_push($output, $result);
            endforeach;

            return json_encode($output);
        endif;
    }
}